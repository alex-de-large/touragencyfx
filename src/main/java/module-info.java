module com.alex.touragency {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.sql;
    requires mysql.connector.java;
    requires static lombok;
    requires org.hibernate.orm.core;

    opens com.alex.touragency.ui.controller to javafx.fxml;
//    opens com.alex.touragency.ui to javafx.fxml;
    exports com.alex.touragency;
}
