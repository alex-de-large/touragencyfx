package com.alex.touragency.ui.controller;

import com.alex.touragency.App;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

import java.io.IOException;

public class LoginController {

    @FXML private TextField loginTextField;
    @FXML private PasswordField passwordField;
    @FXML private Button loginButton;

    public LoginController() {

    }

    @FXML
    private void login(ActionEvent actionEvent) {
        String login = loginTextField.getText();
        String password = passwordField.getText();

        // TODO get user from DB

        try {
            App.setRoot("");
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }
}
