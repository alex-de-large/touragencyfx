package com.alex.touragency.model;

public class UserSession {

    private static final UserSession USER_SESSION = new UserSession();

    private User user;

    private UserSession() {

    }

    public static UserSession get() {
        return USER_SESSION;
    }


    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
