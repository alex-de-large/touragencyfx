package com.alex.touragency.model;

import lombok.*;

import java.sql.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class User {

    int id;
    private String login;
    private String password;
    private String name;
    private String surname;
    private String patronymic;
    private Date birthdate;
    private String email;
    private String phoneNumber;
    private String gender;
    private Role role;

}
