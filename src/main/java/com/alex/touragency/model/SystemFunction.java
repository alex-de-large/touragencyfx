package com.alex.touragency.model;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class SystemFunction {

    private int id;
    private String systemName;
    private String name;

}
