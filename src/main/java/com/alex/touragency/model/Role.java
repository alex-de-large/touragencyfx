package com.alex.touragency.model;

import lombok.*;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class Role {

    private int id;
    private String systemName;
    private String name;
    private List<SystemFunction> systemFunctions;

}
