package com.alex.touragency.db;

import java.util.List;

public interface DAO<I, E> {

    List<E> findAll();

    E findById(I id);

    void update(E entity);

    void delete(I id);

    void create(E id);

}
